export interface Note {
  name?: string;
  idInOctave?: number;
  octave?: number;
  noteFrequency: number;
}

const notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

// returns the closest note to a pitch
export function frequencyToNote(frequency: number): Note {
  if (frequency <= 0) {
    return {
      noteFrequency: 0,
    };
  }
  const midi = Math.round(12 * (Math.log(frequency / 440) / Math.log(2))) + 69;
  const noteFrequency = midiToFrequency(midi);
  const idInOctave = midi % 12;
  const name = notes[idInOctave];
  const octave = Math.floor(midi / 12) - 1;
  return { name, noteFrequency, octave, idInOctave };
}

// returns frequency of note
export function noteIdToNote(idInOctave: number, octave: number): Note {
  const midi = 12 * (octave + 1) + idInOctave;
  const noteFrequency = midiToFrequency(midi);
  const name = notes[idInOctave];
  return { name, noteFrequency, octave, idInOctave };
}

// returns frequency of note in midi
export function midiToFrequency(midi: number) {
  let frequency = 440 * Math.pow(2, (midi - 69) / 12);
  return Math.round(frequency * 100) / 100;
}
