export const CANVAS_HEIGHT = 255;
export const CANVAS_WIDTH_PERCENT = 0.96;
export const BAR_WIDTH = 10.28;

export const FFT_SIZE = 16384;
export const FFT_SMOOTHING = 0.98;
export const FFT_PERIOD = 15;
export const FFT_MIN_DECIBELS = -100;
export const FFT_MAX_DECIBELS = -45;

export const IIR_FEED_FORWARD = [1, -0.97];
export const IIR_FEED_BACKWARD = [1];

export const COMPRESSOR_THRESHOLD = -16;

export const START_STOP_TRANSITION_TIME = 0.3;

export const START_FREQ_DOMINANT_OCTAVE = 4;
export const MAIN_FREQ_DOMINANT_OCTAVE = 4;
export const END_FREQ_DOMINANT_OCTAVE = 6;
export const AUTO_DOMINANT_FREQ_SMOOTHING = 0.9;
export const AUTO_DOMINANT_FREQ_NORMALIZATION = 8;

export const START_FREQ_AMPLITUDE = 200;
export const END_FREQ_AMPLITUDE = 5000;

export const TONE_VOLUME_MIN = 0;
export const TONE_VOLUME_MAX = 0.6;
export const TONE_VOLUME_DEFAULT = 0.1;
export const TONE_VOLUME_AUTO_SMOOTHING = 0.9;
export const TONE_VOLUME_AUTO_MIN_VALUE = 0.05;
export const TONE_VOLUME_AUTO_REDUCTION = 700;

export const TONE_FREQ_MIN = 130;
export const TONE_FREQ_MAX = 1000;
export const TONE_FREQ_DEFAULT = 440;

export const TONE_FREQ_DIFF_MIN = 1;
export const TONE_FREQ_DIFF_MAX = 45;
export const TONE_FREQ_DIFF_DEFAULT = 10;

export const MAIN_VOLUME_MIN = 0;
export const MAIN_VOLUME_MAX = 1;
export const MAIN_VOLUME_DEFAULT = 0.8;

export const SONGS_FOLDER = "songs";
export const DEFAULT_SONGS = ["Itro & Tobu - Cloud 9.mp3"];

export const COLOR_BACKGROUND = "rgb(251, 247, 244)";
export const COLOR_PRIMARY_LIGHT = "rgb(155, 175, 199)";
export const COLOR_PRIMARY_DARK = "rgb(80, 92, 104)";
export const CANVAS_DOMINANT_NOTE_HEIGHT = 10;
