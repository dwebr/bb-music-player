export class Oscillator {
  audioCtx: AudioContext;
  output: AudioNode;
  freq = 440;
  panner: StereoPannerNode;
  oscillatorNode: OscillatorNode;

  constructor(audioCtx: AudioContext, output: AudioNode, pan: number) {
    this.audioCtx = audioCtx;
    this.output = output;
    this.panner = new StereoPannerNode(audioCtx, { pan });
    this.panner.connect(output);
  }

  // starts playing
  public start() {
    this.oscillatorNode = this.audioCtx.createOscillator();
    this.oscillatorNode.frequency.setValueAtTime(
      this.freq,
      this.audioCtx.currentTime
    );
    this.oscillatorNode.connect(this.panner);
    this.oscillatorNode.start();
  }

  // stops playing
  public stop(time?: number) {
    if (time) {
      this.oscillatorNode.stop(this.audioCtx.currentTime + time);
    } else {
      this.oscillatorNode.stop();
    }
  }

  // sets frequency of oscillator
  public setFreq(freq: number) {
    this.freq = freq;
    if (this.oscillatorNode != undefined) {
      this.oscillatorNode.frequency.setValueAtTime(
        freq,
        this.audioCtx.currentTime
      );
    }
  }
}
