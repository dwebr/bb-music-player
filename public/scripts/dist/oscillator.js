export class Oscillator {
    constructor(audioCtx, output, pan) {
        this.freq = 440;
        this.audioCtx = audioCtx;
        this.output = output;
        this.panner = new StereoPannerNode(audioCtx, { pan });
        this.panner.connect(output);
    }
    // starts playing
    start() {
        this.oscillatorNode = this.audioCtx.createOscillator();
        this.oscillatorNode.frequency.setValueAtTime(this.freq, this.audioCtx.currentTime);
        this.oscillatorNode.connect(this.panner);
        this.oscillatorNode.start();
    }
    // stops playing
    stop(time) {
        if (time) {
            this.oscillatorNode.stop(this.audioCtx.currentTime + time);
        }
        else {
            this.oscillatorNode.stop();
        }
    }
    // sets frequency of oscillator
    setFreq(freq) {
        this.freq = freq;
        if (this.oscillatorNode != undefined) {
            this.oscillatorNode.frequency.setValueAtTime(freq, this.audioCtx.currentTime);
        }
    }
}
