var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { getFreqClassByFreq } from "./freqClass.js";
import { frequencyToNote, noteIdToNote, midiToFrequency, } from "./note.js";
import { Oscillator } from "./oscillator.js";
import { Song } from "./song.js";
import { extractFileName, formateSeconds, getExpValue, getLinValue, gaussianFunction, roundExpNumber, } from "./utils.js";
import * as con from "./constants.js";
//--------------- creating objects and initializing constants and variables ---------------
const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
const mainGainNode = audioCtx.createGain();
mainGainNode.gain.setValueAtTime(0, audioCtx.currentTime);
const tonesGainNode = audioCtx.createGain();
tonesGainNode.gain.setValueAtTime(con.TONE_VOLUME_DEFAULT, audioCtx.currentTime);
const oscL = new Oscillator(audioCtx, tonesGainNode, -1);
const oscR = new Oscillator(audioCtx, tonesGainNode, 1);
const fftSmoothing = con.FFT_SMOOTHING;
const analyzerNode = audioCtx.createAnalyser();
analyzerNode.fftSize = con.FFT_SIZE;
analyzerNode.minDecibels = con.FFT_MIN_DECIBELS;
analyzerNode.maxDecibels = con.FFT_MAX_DECIBELS;
analyzerNode.smoothingTimeConstant = fftSmoothing;
const bufferLength = analyzerNode.frequencyBinCount;
const freqDataArray = new Uint8Array(bufferLength);
const binFreqRange = audioCtx.sampleRate / con.FFT_SIZE;
const midiAnalyzeStart = (con.START_FREQ_DOMINANT_OCTAVE + 1) * 12;
const freqAnalyzeStart = (midiToFrequency(midiAnalyzeStart) + midiToFrequency(midiAnalyzeStart - 1)) /
    2;
const binIdAnalyzeStart = freqToBin(freqAnalyzeStart, "up");
const midiAnalyzeEnd = (con.END_FREQ_DOMINANT_OCTAVE + 1) * 12 + 11;
const freqAnalyzeEnd = (midiToFrequency(midiAnalyzeEnd) + midiToFrequency(midiAnalyzeEnd + 1)) / 2;
const binIdAnalyzeEnd = freqToBin(freqAnalyzeEnd, "down");
const binIdAmplitudeStart = freqToBin(con.START_FREQ_AMPLITUDE);
const binIdAmplitudeEnd = freqToBin(con.END_FREQ_AMPLITUDE);
let maxNoteOctaveId;
let notesSumValues = [];
const dynamicsCompressorNode = new DynamicsCompressorNode(audioCtx, {
    threshold: con.COMPRESSOR_THRESHOLD,
});
const iirFilterNode = audioCtx.createIIRFilter(con.IIR_FEED_FORWARD, con.IIR_FEED_BACKWARD);
let autoToneVolume = true;
let autoToneFreq = true;
const songs = [];
let currentSong = undefined;
let songTimeUserInput = false;
let barWidth;
//--------------- binding HTML elements ---------------
const body = document.querySelector("body");
const canvas = document.querySelector("canvas");
const canvasCtx = canvas.getContext("2d");
const fileSelector = document.getElementById("fileSelector");
const loadingDiv = document.getElementById("loading");
const playIcon = document.getElementById("playIcon");
const playButton = document.getElementById("playButton");
const previousButton = (document.getElementById("previousButton"));
const nextButton = document.getElementById("nextButton");
const songNameLabel = (document.getElementById("songNameLabel"));
const mainVolumeSlider = (document.getElementById("mainVolumeSlider"));
const songSlider = document.getElementById("songSlider");
const currTimeLabel = (document.getElementById("currTimeLabel"));
const maxTimeLabel = document.getElementById("maxTimeLabel");
const toneFreqSlider = (document.getElementById("toneFreqSlider"));
const maxFreqLabel = document.getElementById("maxFreqLabel");
const minFreqLabel = document.getElementById("minFreqLabel");
const toneFreqLabel = (document.getElementById("toneFreqLabel"));
const toneNameLabel = (document.getElementById("toneNameLabel"));
const toneFreqDiffSlider = (document.getElementById("toneFreqDiffSlider"));
const toneFreqDiffLabel = (document.getElementById("toneFreqDiffLabel"));
const toneFreqDiffMinLabel = (document.getElementById("minFreqDiffLabel"));
const toneFreqDiffMaxLabel = (document.getElementById("maxFreqDiffLabel"));
const toneFreqClassLabel = (document.getElementById("toneFreqClassLabel"));
const toneVolumeSlider = (document.getElementById("toneVolumeSlider"));
const checkboxAutoVolumeTone = (document.getElementById("checkboxAutoVolumeTone"));
const checkboxAutoFreq = (document.getElementById("checkboxAutoFreq"));
//--------------- setting canvas size ---------------
setCanvasWidth();
canvas.height = con.CANVAS_HEIGHT;
//--------------- setting default values ---------------
minFreqLabel.innerHTML = con.TONE_FREQ_MIN + " Hz";
maxFreqLabel.innerHTML = con.TONE_FREQ_MAX + " Hz";
toneFreqDiffMinLabel.innerHTML = con.TONE_FREQ_DIFF_MIN + " Hz";
toneFreqDiffMaxLabel.innerHTML = con.TONE_FREQ_DIFF_MAX + " Hz";
setFrequencyDiffLabels(con.TONE_FREQ_DIFF_DEFAULT);
setOscillatorsBaseFreq(frequencyToNote(con.TONE_FREQ_DEFAULT));
setMainVolumeSliderValue(con.MAIN_VOLUME_DEFAULT);
setToneFreqSliderValue(con.TONE_FREQ_DEFAULT);
setToneFreqDiffSliderValue(con.TONE_FREQ_DIFF_DEFAULT);
setToneVolumeSliderValue(con.TONE_VOLUME_DEFAULT);
//--------------- setting listeners ---------------
window.onresize = function () {
    setCanvasWidth();
};
fileSelector.addEventListener("change", function () {
    if (this.files[0] == undefined) {
        return;
    }
    setLoading(true);
    setupUserSamples(this.files).then(() => {
        setLoading(false);
    });
}, false);
songSlider.addEventListener("change", function () {
    songTimeUserInput = false;
    currentSong.jump(+songSlider.value);
}, false);
songSlider.addEventListener("input", function () {
    songTimeUserInput = true;
    currTimeLabel.innerHTML = formateSeconds(+songSlider.value);
}, false);
playButton.addEventListener("click", function () {
    if (currentSong.isPlaying()) {
        stopAudio();
    }
    else {
        startAudio();
    }
}, false);
previousButton.addEventListener("click", loadPreviousSong, false);
nextButton.addEventListener("click", loadNextSong, false);
mainVolumeSlider.addEventListener("input", function () {
    const value = getMainVolumeSliderValue();
    mainGainNode.gain.setValueAtTime(value, audioCtx.currentTime);
}, false);
toneFreqSlider.addEventListener("input", function () {
    if (autoToneFreq) {
        autoToneFreq = false;
        checkboxAutoFreq.checked = false;
    }
    const note = getFreqSliderNote();
    prevMaxNoteId = getFreqSliderNote().idInOctave;
    maxNoteOctaveId = note.octave;
    setOscillatorsBaseFreq(note);
}, false);
toneFreqDiffSlider.addEventListener("input", setOscillatorsDiffFreq, false);
toneVolumeSlider.addEventListener("input", function () {
    if (autoToneVolume) {
        autoToneVolume = false;
        checkboxAutoVolumeTone.checked = false;
    }
    const value = getToneVolumeSliderValue();
    tonesGainNode.gain.setValueAtTime(value, audioCtx.currentTime);
}, false);
checkboxAutoVolumeTone.addEventListener("change", function () {
    autoToneVolume = this.checked;
}, false);
checkboxAutoFreq.addEventListener("change", function () {
    autoToneFreq = this.checked;
}, false);
//--------------- defining functions ---------------
//---- configuring oscillators ----
// sets oscillators base frequency
function setOscillatorsBaseFreq(note) {
    if (note.noteFrequency == 0) {
        toneFreqLabel.innerHTML = 0 + " Hz";
        toneNameLabel.innerHTML = "-";
    }
    else {
        const freqDifference = getFreqDiffSliderValue();
        const valueL = note.noteFrequency - freqDifference / 2;
        const valueR = note.noteFrequency + freqDifference / 2;
        oscL.setFreq(valueL);
        oscR.setFreq(valueR);
        toneFreqLabel.innerHTML = note.noteFrequency + " Hz";
        toneNameLabel.innerHTML = note.name.toString() + note.octave;
    }
}
// sets oscillators frequencies difference
function setOscillatorsDiffFreq() {
    const note = getFreqSliderNote();
    const diffValue = getFreqDiffSliderValue();
    const valueL = note.noteFrequency - diffValue / 2;
    const valueR = note.noteFrequency + diffValue / 2;
    oscL.setFreq(valueL);
    oscR.setFreq(valueR);
    setFrequencyDiffLabels(diffValue);
}
//---- components getters and setters ----
function setCanvasWidth() {
    const width = body.getBoundingClientRect().width * con.CANVAS_WIDTH_PERCENT;
    canvas.width = width;
    barWidth = width / (binIdAnalyzeEnd - binIdAnalyzeStart);
}
function setFrequencyDiffLabels(value) {
    toneFreqDiffLabel.innerHTML = value + " Hz";
    const freqClass = getFreqClassByFreq(value);
    toneFreqClassLabel.innerHTML = freqClass;
}
function setToneVolumeSliderValue(value) {
    toneVolumeSlider.value =
        "" + getLinValue(value, con.TONE_VOLUME_MIN, con.TONE_VOLUME_MAX);
}
function setMainVolumeSliderValue(value) {
    mainVolumeSlider.value =
        "" + getLinValue(value, con.MAIN_VOLUME_MIN, con.MAIN_VOLUME_MAX);
}
function setToneFreqSliderValue(value) {
    toneFreqSlider.value =
        "" + getLinValue(value, con.TONE_FREQ_MIN, con.TONE_FREQ_MAX);
}
function setToneFreqDiffSliderValue(value) {
    toneFreqDiffSlider.value =
        "" + getLinValue(value, con.TONE_FREQ_DIFF_MIN, con.TONE_FREQ_DIFF_MAX);
}
function getFreqDiffSliderValue() {
    const value = getExpValue(+toneFreqDiffSlider.value, con.TONE_FREQ_DIFF_MIN, con.TONE_FREQ_DIFF_MAX);
    const roundedValue = roundExpNumber(value);
    return roundedValue;
}
function getFreqSliderNote() {
    return frequencyToNote(getExpValue(+toneFreqSlider.value, con.TONE_FREQ_MIN, con.TONE_FREQ_MAX));
}
function getToneVolumeSliderValue() {
    return getExpValue(+toneVolumeSlider.value, con.TONE_VOLUME_MIN, con.TONE_VOLUME_MAX);
}
function getMainVolumeSliderValue() {
    return getExpValue(+mainVolumeSlider.value, con.MAIN_VOLUME_MIN, con.MAIN_VOLUME_MAX);
}
//---- loading files ----
// loads sound
function getSample(name, file) {
    return __awaiter(this, void 0, void 0, function* () {
        const arrayBuffer = yield file.arrayBuffer();
        const audioBuffer = yield audioCtx.decodeAudioData(arrayBuffer);
        return new Song(name, audioBuffer, audioCtx, [mainGainNode, iirFilterNode], loadNextSong);
    });
}
// setup default samples
function setupDefaultSamples() {
    return __awaiter(this, void 0, void 0, function* () {
        for (let songName of con.DEFAULT_SONGS) {
            const response = yield fetch(con.SONGS_FOLDER + "/" + songName);
            const name = extractFileName(songName);
            const fileBlob = yield response.blob();
            const song = yield getSample(name, fileBlob);
            songs.push(song);
        }
        switchSong(0);
    });
}
// setups user samples
function setupUserSamples(files) {
    return __awaiter(this, void 0, void 0, function* () {
        if (currentSong.isPlaying()) {
            stopAudio();
        }
        songs.length = 0;
        for (let file of files) {
            const name = extractFileName(file.name);
            const song = yield getSample(name, file);
            songs.push(song);
        }
        switchSong(0);
    });
}
//---- switching songs ----
// displays next song
function loadNextSong() {
    const nextSongId = (songs.indexOf(currentSong) + 1) % songs.length;
    switchSong(nextSongId);
}
// displays next song
function loadPreviousSong() {
    const currentSongId = songs.indexOf(currentSong);
    const nextSongId = currentSongId - 1 < 0 ? songs.length - 1 : currentSongId - 1;
    switchSong(nextSongId);
}
// displays song by songId
function switchSong(songId) {
    let startPlaying = false;
    if (currentSong != undefined) {
        if (currentSong.isPlaying()) {
            startPlaying = true;
        }
        currentSong.stop();
    }
    currentSong = songs[songId];
    maxTimeLabel.innerHTML = formateSeconds(currentSong.getDuration());
    songSlider.max = currentSong.getDuration() + "";
    currTimeLabel.innerHTML = formateSeconds(0);
    songSlider.value = 0 + "";
    songNameLabel.innerHTML = currentSong.getName();
    if (startPlaying) {
        currentSong.play();
    }
}
//---- start stop audio ----
// stops playing audio
function stopAudio() {
    currentSong.pause(con.START_STOP_TRANSITION_TIME);
    oscL.stop(con.START_STOP_TRANSITION_TIME);
    oscR.stop(con.START_STOP_TRANSITION_TIME);
    mainGainNode.gain.linearRampToValueAtTime(0.0001, audioCtx.currentTime + con.START_STOP_TRANSITION_TIME);
    notesSumValues.fill(0);
    togglePlayIcon();
}
// starts playing audio
function startAudio() {
    oscL.start();
    oscR.start();
    currentSong.play();
    const gain = getMainVolumeSliderValue();
    mainGainNode.gain.linearRampToValueAtTime(gain, audioCtx.currentTime + con.START_STOP_TRANSITION_TIME);
    togglePlayIcon();
}
// toggle play/pause icon
function togglePlayIcon() {
    playIcon.classList.toggle("fa-pause");
    playIcon.classList.toggle("fa-play");
}
//---- loading ----
// displays/hides loading animation
function setLoading(loading) {
    if (loading) {
        loadingDiv.classList.remove("hidden");
        loadingDiv.classList.add("flex");
    }
    else {
        loadingDiv.classList.remove("flex");
        loadingDiv.classList.add("hidden");
    }
}
//---- frequency analysis ----
// maps bin of FFT to frequency
function binToFreq(binId) {
    return binFreqRange * (binId - 1 / 2);
}
// maps frequency to bin of FFT
function freqToBin(freq, rounding = "nearest") {
    const bin = freq / binFreqRange + 1 / 2;
    if (rounding == "up") {
        return Math.ceil(bin);
    }
    else if (rounding == "down") {
        return Math.floor(bin);
    }
    return Math.round(bin);
}
// sets amplitude based on current playing sound
function setAutoAmplitude() {
    let amplitude = 0;
    for (let i = binIdAmplitudeStart; i <= binIdAmplitudeEnd; i++) {
        amplitude += freqDataArray[i];
    }
    if (amplitude > 0) {
        amplitude =
            con.TONE_VOLUME_AUTO_MIN_VALUE +
                amplitude /
                    ((binIdAmplitudeEnd - binIdAmplitudeStart) *
                        con.TONE_VOLUME_AUTO_REDUCTION);
        amplitude =
            amplitude * (1 - con.TONE_VOLUME_AUTO_SMOOTHING) +
                prevAmplitude * con.TONE_VOLUME_AUTO_SMOOTHING;
        prevAmplitude = amplitude;
        setToneVolumeSliderValue(amplitude);
        tonesGainNode.gain.setValueAtTime(amplitude, audioCtx.currentTime);
    }
}
// analyzes averages intensities of notes in octaves
function analyzeNotesInOctaves() {
    let notesValues = new Array(12);
    let notesCount = new Array(12);
    let notesValuesOctave = new Array(12);
    notesValues.fill(0);
    notesCount.fill(0);
    notesValuesOctave.fill(0);
    let currOctave = con.START_FREQ_DOMINANT_OCTAVE;
    for (let binId = binIdAnalyzeStart; binId < binIdAnalyzeEnd; binId++) {
        const note = frequencyToNote(binToFreq(binId));
        const value = freqDataArray[binId];
        if (note.octave > currOctave) {
            for (let index = 0; index < notesValues.length; index++) {
                if (notesCount[index] > 0) {
                    notesValues[index] += notesValuesOctave[index] / notesCount[index];
                }
            }
            notesCount.fill(0);
            notesValuesOctave.fill(0);
            currOctave++;
        }
        notesValuesOctave[note.idInOctave] += value;
        notesCount[note.idInOctave]++;
    }
    // process notes data
    for (let index = 0; index < notesValues.length; index++) {
        notesValues[index] += notesValuesOctave[index] / notesCount[index];
        let value = notesValues[index];
        // avg
        value /= con.END_FREQ_DOMINANT_OCTAVE - con.START_FREQ_DOMINANT_OCTAVE + 1;
        //normalization
        const normalDistValue = gaussianFunction(index, prevMaxNoteId, con.AUTO_DOMINANT_FREQ_NORMALIZATION);
        value *= normalDistValue;
        notesValues[index] = value;
    }
    return notesValues;
}
// smooth dominant note value with previous value
function smoothDominantNote(noteId) {
    noteId =
        (1 - con.AUTO_DOMINANT_FREQ_SMOOTHING) * noteId +
            con.AUTO_DOMINANT_FREQ_SMOOTHING * prevMaxNoteId;
    prevMaxNoteId = noteId;
    return Math.round(noteId);
}
// analyze current playing sound
function dynamicSoundAnalysis() {
    analyzerNode.getByteFrequencyData(freqDataArray);
    //change song time slider progress
    if (currentSong == undefined || !currentSong.isPlaying()) {
        return;
    }
    //auto volume
    if (autoToneVolume) {
        setAutoAmplitude();
    }
    if (autoToneFreq) {
        notesSumValues = analyzeNotesInOctaves();
        const maxValue = Math.max(...notesSumValues);
        let maxNoteId = notesSumValues.indexOf(maxValue);
        // smoothing max value
        if (maxValue > 0) {
            maxNoteId = smoothDominantNote(maxNoteId);
        }
        else {
            maxNoteId = getFreqSliderNote().idInOctave;
        }
        // set oscillators to current note
        const note = noteIdToNote(maxNoteId, con.MAIN_FREQ_DOMINANT_OCTAVE);
        setOscillatorsBaseFreq(note);
        setToneFreqSliderValue(note.noteFrequency);
        maxNoteOctaveId = con.MAIN_FREQ_DOMINANT_OCTAVE;
    }
}
// animation
function draw() {
    // request new animation frame
    requestAnimationFrame(draw);
    if (currentSong == undefined) {
        return;
    }
    //change song time slider progress
    if (!songTimeUserInput) {
        songSlider.value = currentSong.getCurrentTime() + "";
        currTimeLabel.innerHTML = formateSeconds(currentSong.getCurrentTime());
    }
    // draw sound spectrum
    drawSpectrum();
}
// draw sound spectrum into canvas
function drawSpectrum() {
    canvasCtx.fillStyle = con.COLOR_BACKGROUND;
    canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
    for (let binId = binIdAnalyzeStart; binId < binIdAnalyzeEnd; binId++) {
        const value = freqDataArray[binId];
        const note = frequencyToNote(binToFreq(binId));
        // draw bin with frequency color
        canvasCtx.fillStyle =
            "rgb(255," +
                (100 + note.idInOctave * 12) +
                "," +
                (200 - 15 * note.idInOctave) +
                ")";
        canvasCtx.fillRect(barWidth * (binId - binIdAnalyzeStart) - 0.2, canvas.height - value, barWidth + 0.4, value);
    }
    if (currentSong.isPlaying()) {
        if (autoToneFreq) {
            canvasCtx.fillStyle = con.COLOR_PRIMARY_LIGHT;
            for (let index = 0; index < 12; index++) {
                drawFrequency(index, con.MAIN_FREQ_DOMINANT_OCTAVE, notesSumValues[index] * 0.8);
            }
        }
        for (let octave = con.START_FREQ_DOMINANT_OCTAVE; octave <= con.END_FREQ_DOMINANT_OCTAVE; octave++) {
            if (maxNoteOctaveId == octave) {
                canvasCtx.fillStyle = con.COLOR_PRIMARY_DARK;
            }
            else {
                canvasCtx.fillStyle = con.COLOR_BACKGROUND;
            }
            drawFrequency(prevMaxNoteId, octave, con.CANVAS_DOMINANT_NOTE_HEIGHT);
        }
    }
}
// draws frequency bar into canvas
function drawFrequency(id, octave, amplitude) {
    const noteInOctave = noteIdToNote(id, octave);
    canvasCtx.fillRect(barWidth * (freqToBin(noteInOctave.noteFrequency) - binIdAnalyzeStart), canvas.height - amplitude, barWidth, amplitude);
}
//--------------- main code ---------------
// loading samples
setupDefaultSamples().then(() => {
    setLoading(false);
    loadingDiv.classList.remove("solidBackground");
    canvas.classList.remove("hidden");
});
//connecting nodes
iirFilterNode.connect(analyzerNode);
tonesGainNode.connect(mainGainNode);
mainGainNode.connect(dynamicsCompressorNode);
dynamicsCompressorNode.connect(audioCtx.destination);
//clear canvas
canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
//animation start
requestAnimationFrame(draw);
//sound analysis start
let prevMaxNoteId = getFreqSliderNote().idInOctave;
let prevAmplitude = getToneVolumeSliderValue();
setInterval(() => dynamicSoundAnalysis(), con.FFT_PERIOD);
