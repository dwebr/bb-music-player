export class Song {
    constructor(name, buffer, audioCtx, outputs, onSoundEnded) {
        this.sourceNode = null;
        this.startedAt = 0;
        this.pausedAt = 0;
        this.playing = false;
        this.name = name;
        this.buffer = buffer;
        this.audioCtx = audioCtx;
        this.outputs = outputs;
        this.onSoundEnded = onSoundEnded;
    }
    // starts playing
    play() {
        this.sourceNode = this.audioCtx.createBufferSource();
        for (let output of this.outputs) {
            this.sourceNode.connect(output);
        }
        this.sourceNode.buffer = this.buffer;
        this.sourceNode.start(0, this.pausedAt);
        this.startedAt = this.audioCtx.currentTime - this.pausedAt;
        this.pausedAt = 0;
        this.playing = true;
        this.sourceNode.onended = this.onSoundStopped.bind(this);
    }
    onSoundStopped(event) {
        if (Math.abs(this.buffer.duration - this.getCurrentTime()) < 0.1) {
            this.onSoundEnded();
        }
    }
    // jumps at time in song
    jump(time) {
        let startPlaying = this.playing;
        this.stop();
        this.pausedAt = time;
        if (startPlaying) {
            this.play();
        }
    }
    // pause song
    pause(time) {
        if (time) {
            var elapsed = this.audioCtx.currentTime - this.startedAt + time;
            this.stop(time);
            this.pausedAt = elapsed;
        }
        else {
            var elapsed = this.audioCtx.currentTime - this.startedAt;
            this.stop();
            this.pausedAt = elapsed;
        }
    }
    // stops songs
    stop(time) {
        if (this.sourceNode) {
            if (time) {
                this.sourceNode.stop(this.audioCtx.currentTime + time);
            }
            else {
                this.sourceNode.stop();
            }
            this.sourceNode = null;
        }
        this.pausedAt = 0;
        this.startedAt = 0;
        this.playing = false;
    }
    isPlaying() {
        return this.playing;
    }
    getCurrentTime() {
        if (this.playing) {
            return this.audioCtx.currentTime - this.startedAt;
        }
        else {
            return this.pausedAt;
        }
    }
    getDuration() {
        return this.buffer.duration;
    }
    getName() {
        return this.name;
    }
}
