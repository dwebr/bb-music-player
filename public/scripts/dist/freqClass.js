export var FreqClass;
(function (FreqClass) {
    FreqClass["GAMMA"] = "GAMMA";
    FreqClass["BETA"] = "BETA";
    FreqClass["ALPHA"] = "ALPHA";
    FreqClass["THETA"] = "THETA";
    FreqClass["DELTA"] = "DELTA";
})(FreqClass || (FreqClass = {}));
// returns frequency class by frequency
export function getFreqClassByFreq(freq) {
    if (freq <= 4) {
        return FreqClass.DELTA;
    }
    else if (freq <= 8) {
        return FreqClass.THETA;
    }
    else if (freq <= 13) {
        return FreqClass.ALPHA;
    }
    else if (freq <= 30) {
        return FreqClass.BETA;
    }
    else {
        return FreqClass.GAMMA;
    }
}
