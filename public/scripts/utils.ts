// returns formatted timestamp
export function formateSeconds(duration: number) {
  var hrs = ~~(duration / 3600);
  var mins = ~~((duration % 3600) / 60);
  var secs = ~~duration % 60;
  var ret = "";
  if (hrs > 0) {
    ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }
  ret += "" + mins + ":" + (secs < 10 ? "0" : "");
  ret += "" + secs;
  return ret;
}

// gets name of file without suffix from path
export function extractFileName(path: string) {
  return path.split(".").slice(0, -1).join().split("/").pop();
}

// returns linear value from range
export function getLinValue(value: number, min: number, max: number) {
  const range = max - min;
  const normalizedValue = (value - min) / range;
  const squareRoot = Math.sqrt(normalizedValue);
  return squareRoot;
}

// returns exponential value from range
export function getExpValue(value: number, min: number, max: number) {
  const range = max - min;
  const expValue = value * value;
  const normalizedValue = expValue * range + min;
  return normalizedValue;
}

export function roundExpNumber(num: number) {
  if (num <= 8) {
    return Math.round(num * 2) / 2.0;
  } else {
    return Math.round(num);
  }
}

// returns value of Gaussian function in point x
export function gaussianFunction(x: number, mean: number, stdDev: number) {
  return Math.exp(-0.5 * Math.pow((x - mean) / stdDev, 2));
}
