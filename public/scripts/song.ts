export class Song {
  onSoundEnded: () => void;
  audioCtx: AudioContext;
  buffer: AudioBuffer;
  outputs: AudioNode[];
  sourceNode: AudioBufferSourceNode = null;
  startedAt = 0;
  pausedAt = 0;
  playing = false;
  name: string;

  constructor(
    name: string,
    buffer: AudioBuffer,
    audioCtx: AudioContext,
    outputs: AudioNode[],
    onSoundEnded: () => void
  ) {
    this.name = name;
    this.buffer = buffer;
    this.audioCtx = audioCtx;
    this.outputs = outputs;
    this.onSoundEnded = onSoundEnded;
  }

  // starts playing
  public play() {
    this.sourceNode = this.audioCtx.createBufferSource();
    for (let output of this.outputs) {
      this.sourceNode.connect(output);
    }
    this.sourceNode.buffer = this.buffer;
    this.sourceNode.start(0, this.pausedAt);

    this.startedAt = this.audioCtx.currentTime - this.pausedAt;
    this.pausedAt = 0;
    this.playing = true;

    this.sourceNode.onended = this.onSoundStopped.bind(this);
  }

  private onSoundStopped(event: Event) {
    if (Math.abs(this.buffer.duration - this.getCurrentTime()) < 0.1) {
      this.onSoundEnded();
    }
  }

  // jumps at time in song
  public jump(time: number) {
    let startPlaying = this.playing;
    this.stop();
    this.pausedAt = time;
    if (startPlaying) {
      this.play();
    }
  }

  // pause song
  public pause(time?: number) {
    if (time) {
      var elapsed = this.audioCtx.currentTime - this.startedAt + time;
      this.stop(time);
      this.pausedAt = elapsed;
    } else {
      var elapsed = this.audioCtx.currentTime - this.startedAt;
      this.stop();
      this.pausedAt = elapsed;
    }
  }

  // stops songs
  public stop(time?: number) {
    if (this.sourceNode) {
      if (time) {
        this.sourceNode.stop(this.audioCtx.currentTime + time);
      } else {
        this.sourceNode.stop();
      }
      this.sourceNode = null;
    }
    this.pausedAt = 0;
    this.startedAt = 0;
    this.playing = false;
  }

  public isPlaying() {
    return this.playing;
  }

  public getCurrentTime() {
    if (this.playing) {
      return this.audioCtx.currentTime - this.startedAt;
    } else {
      return this.pausedAt;
    }
  }

  public getDuration() {
    return this.buffer.duration;
  }

  public getName() {
    return this.name;
  }
}
