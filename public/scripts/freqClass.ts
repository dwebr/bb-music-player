export enum FreqClass {
  GAMMA = "GAMMA",
  BETA = "BETA",
  ALPHA = "ALPHA",
  THETA = "THETA",
  DELTA = "DELTA",
}

// returns frequency class by frequency
export function getFreqClassByFreq(freq: number) {
  if (freq <= 4) {
    return FreqClass.DELTA;
  } else if (freq <= 8) {
    return FreqClass.THETA;
  } else if (freq <= 13) {
    return FreqClass.ALPHA;
  } else if (freq <= 30) {
    return FreqClass.BETA;
  } else {
    return FreqClass.GAMMA;
  }
}
